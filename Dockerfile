FROM alpine:3.4

MAINTAINER Mike Terzo <mike@terzo.org>

RUN apk add --no-cache tftp-hpa syslinux

RUN mkdir -p /tftp/pxelinux.cfg /tftp/dists

RUN for i in /usr/share/syslinux/*; do \
       if [ -f $i ]; then              \
           ln $i /tftp/$(basename $i); \
       fi;                             \
    done

EXPOSE 69/udp

ENTRYPOINT ["/usr/sbin/in.tftpd", "--foreground", "--secure", "-a", "0:69", \
            "-vv"]
CMD [ "/tftp" ]
